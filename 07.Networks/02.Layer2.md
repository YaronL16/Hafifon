### Goals
- The trainee will get familiar with layer 2 of the osi model
- The trainee will also get to know few linux commands regarding layer 2

### Tasks
- What is the responsibility of the second layer?
- Explain what are MAC addresses, the structure of MAC addresses and why are they needed
- Explain in your own words what are broadcast domains; why would we like to control their sizes and what method will we use to limit the size of a broadcast domain
- Explain in your own words what are collision domains; explain how can we "break" a collision domain into smaller collision domains
- Hubs, Bridges and Switches
  - Why do we need a central network device to connect to?
  - Explain how do network hubs transmit packets in lan environment
  - Explain what are the differences between network hubs and network switches
  - How does a network switch know to what port to transmit an incoming packet to?
  - Explain what is the main use of a network bridge
- Give an example for proper use of VLANs in a LAN environment
- What are the different VLAN types?
- In a network switch, when would we use Access switchport and when would we use Trunk switchport? give example for each one
- Explain how does 802.1q (VLAN tagging) works
- Explain what is MTU, and how does it effect the network traffic
- In Linux, how can you check the MAC address and MTU that is configured on each of the network interfaces?
- Read about network topology
- What are some common data link standards?
